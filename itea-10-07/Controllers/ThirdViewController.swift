//
//  ThirdViewController.swift
//  itea-10-07
//
//  Created by Валентин Петруля on 7/10/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import UIKit
import Foundation
import WebKit

class ThirdViewController: UIViewController {
    
    var news: News?
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        titleLabel.text = news?.name
        if let url = URL(string: news!.url!) {
            webView.load(URLRequest(url: url))
        }
    }
    
    
    @IBAction func didTapBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
