//
//  NewsModel.swift
//  itea-10-07
//
//  Created by Валентин Петруля on 7/10/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import Foundation

class News {
    var id: String?
    var name: String?
    var description: String?
    var url: String?
    var category: String?
    var language: String?
    var country: String?
}
