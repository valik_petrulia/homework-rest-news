//
//  FirstViewController.swift
//  itea-10-07
//
//  Created by Валентин Петруля on 7/10/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    @IBOutlet weak var startButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startButton.layer.cornerRadius = 15
        // Do any additional setup after loading the view.
    }

    @IBAction func didTapStartButton(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as! SecondViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
