//
//  NewsCell.swift
//  itea-10-07
//
//  Created by Валентин Петруля on 7/10/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import UIKit

protocol SiteDelegate {
    func showSite(index: Int, section: Int)
}

class NewsCell: UITableViewCell {
    //var news: News?
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var siteButton: UIButton!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var delegate: SiteDelegate?
    var sectionIndex: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func update(news: News) {
        nameLabel.text = news.name
        siteButton.setTitle(news.url, for: .normal)
        categoryLabel.text = news.category
        descriptionLabel.text = news.description
    }
    
    @IBAction func didTapSiteButton(_ sender: Any) {
        if let section = sectionIndex {
            delegate?.showSite(index: self.tag, section: section)
        }
    }
    
}
