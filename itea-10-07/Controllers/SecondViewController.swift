//
//  SecondViewController.swift
//  itea-10-07
//
//  Created by Валентин Петруля on 7/10/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    var newsList: [News] = []
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        parcing()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "NewsCell", bundle: nil), forCellReuseIdentifier: "NewsCell")
        // Do any additional setup after loading the view.
    }
    
    func parcing() {
        let url = URL(string: "https://newsapi.org/v2/sources")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = ["X-Api-Key":"1cbf132ed3be4efa940d7137a7432736"]
        
        let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
            guard let data = data else {
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    if let sources = json["sources"] as? [[String: String]] {
                        for source in sources {
                            let news = News()
                            if let id = source["id"] {
                                news.id = id
                            }
                            if let name = source["name"] {
                                news.name = name
                            }
                            if let description = source["description"] {
                                news.description = description
                            }
                            if let url = source["url"] {
                                news.url = url
                            }
                            if let category = source["category"] {
                                news.category = category
                            }
                            if let language = source["language"] {
                                news.language = language
                            }
                            if let country = source["country"] {
                                news.country = country
                            }
                            self.newsList.append(news)
                            print(self.newsList.count)
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            } catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
            }
        }
        task.resume()

    }
    
}

extension SecondViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(newsList.count)
        return newsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as! NewsCell
        cell.tag = indexPath.row
        cell.sectionIndex = indexPath.section
        cell.delegate = self
        cell.update(news: newsList[indexPath.row])
        return cell
    }
}

extension SecondViewController: SiteDelegate {
    func showSite(index: Int, section: Int) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ThirdViewController") as! ThirdViewController
        vc.news = newsList[index]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
